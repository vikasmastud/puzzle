package mini.project.puzzle;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.Random;

public class MainActivity extends AppCompatActivity implements View.OnTouchListener, GestureDetector.OnGestureListener {

    private GestureDetector gestureDetector;
    ImageView actionImageView;
    ImageView[][] puzzle;
    int[][] imges;
    ImageView b1, b2, b3, b4, b5, b6, b7, b8, b9;
    LinearLayout layout;
    public static final int SWIPE_THRESHOLD = 100;
    public static final int SWIPE_VELOCITY_THRESHOLD = 100;
    int x = 0;
    int y = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imges = new int[3][3];
        layout = findViewById(R.id.layout);

        b1 = findViewById(R.id.btn1);
        b2 = findViewById(R.id.btn2);
        b3 = findViewById(R.id.btn3);

        b4 = findViewById(R.id.btn4);
        b5 = findViewById(R.id.btn5);
        b6 = findViewById(R.id.btn6);

        b7 = findViewById(R.id.btn7);
        b8 = findViewById(R.id.btn8);
        b9 = findViewById(R.id.btn9);


        puzzle = new ImageView[3][3];
        puzzle[0][0] = b1;
        puzzle[0][1] = b2;
        puzzle[0][2] = b3;
        puzzle[1][0] = b4;
        puzzle[1][1] = b5;
        puzzle[1][2] = b6;
        puzzle[2][0] = b7;
        puzzle[2][1] = b8;
        puzzle[2][2] = b9;

        init();
        display();
        gestureDetector = new GestureDetector(this,this);
        actionImageView = getActionImageView();

        for(int i = 0; i<3 ;i++){
            for(int j=0; j < 3 ;j++){
                puzzle[i][j].setOnTouchListener(this);
            }
        }
    }

    @Override
    public boolean onDown(MotionEvent motionEvent) {
        return false;
    }

    @Override
    public void onShowPress(MotionEvent motionEvent) {

    }

    @Override
    public boolean onSingleTapUp(MotionEvent motionEvent) {
        return false;
    }

    @Override
    public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent1, float v, float v1) {
        return false;
    }

    @Override
    public void onLongPress(MotionEvent motionEvent) {

    }

    @Override
    public boolean onFling(MotionEvent downEvent, MotionEvent moveEvent, float velocityX, float velocityY) {
        boolean result = false;
        float diffY = moveEvent.getY() - downEvent.getY();
        float diffX = moveEvent.getX() - downEvent.getX();
        // which was greater?  movement across Y or X?
        if (Math.abs(diffX) > Math.abs(diffY)) {
            // right or left swipe
            if (Math.abs(diffX)> SWIPE_THRESHOLD && Math.abs(velocityX) > SWIPE_VELOCITY_THRESHOLD) {
                if (diffX > 0) {
                    result = onSwipeRight();
                } else {
                    result = onSwipeLeft();
                }
//                result = true;
            }
        } else {
            // up or down swipe
            if (Math.abs(diffY) > SWIPE_THRESHOLD && Math.abs(velocityY)> SWIPE_VELOCITY_THRESHOLD) {
                if (diffY > 0) {
                    result = onSwipeBottom();
                } else {
                    result = onSwipeTop();
                }
//                result = true;
            }
        }

        return result;
    }
    private boolean onSwipeTop() {
        Log.i("swip top", x + " "+ y);
        if(x == 2) return true;
        Drawable curr = puzzle[x][y].getBackground();
        Drawable next = puzzle[x+1][y].getBackground();
        puzzle[x][y].setBackground(next);
        puzzle[x+1][y].setBackground(curr);
        x++;

        return true;
    }

    private boolean onSwipeBottom() {
        Log.i("swip Bottom", x + " "+ y);

        if(x == 0) return true;
        Drawable curr = puzzle[x][y].getBackground();
        Drawable next = puzzle[x-1][y].getBackground();
        puzzle[x][y].setBackground(next);
        puzzle[x-1][y].setBackground(curr);
        x--;

        return true;
    }

    private boolean onSwipeLeft() {
        Log.i("swip left", x + " "+ y);

        if(y == 2) return true;
//        int tmp = imges[x][y];
//        imges[x][y] = imges[x][y+1];
//        imges[x][y+1] = tmp;
//        y++;
//        display();
        Drawable curr = puzzle[x][y].getBackground();
        Drawable next = puzzle[x][y+1].getBackground();
        puzzle[x][y].setBackground(next);
        puzzle[x][y+1].setBackground(curr);
        y++;
        return true;
    }

    private boolean onSwipeRight() {
        Log.i("swip right", x + " "+ y);

        if(y == 0) return true;
//        int tmp = imges[x][y];
//        imges[x][y] = imges[x][y-1];
//        imges[x][y-1] = tmp;
        Drawable curr = puzzle[x][y].getBackground();
        Drawable next = puzzle[x][y-1].getBackground();
        puzzle[x][y].setBackground(next);
        puzzle[x][y-1].setBackground(curr);
        y--;
//        display();
        return true;

    }

    public ImageView getActionImageView(){
        for(int i = 0; i < 3 ; i++){
            for(int j = 0; j < 3 ; j++){
                if(puzzle[i][j].getBackground().getConstantState().equals(
                        getResources().getDrawable(R.drawable.p3,null).getConstantState())){
                    x = i;
                    y = j;
                    return puzzle[i][j];
                }
            }
        }
        return puzzle[0][0];
    }

    public void init() {
        ArrayList<Integer> list = new ArrayList<>();
        for (int i = 1; i < 10; i++) list.add(i);
        for (int i = 0; i < 9; i++) {
            Random rnd = new Random();
            int r = rnd.nextInt(9);
            if (r > 1 && r < 8) {
                int a = list.get(r);
                int b = list.get(r-1);
                int c = list.get(r+1);

                a = a^b;
                b = a^b;
                a = a^b;

                b = b^c;
                c = b^c;
                b = b^c;

                list.set(r, a);
                list.set(r-1 , b);
                list.set(r+1 , c);
            }
        }

        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                int num = list.remove(0);
                imges[i][j] = getImage(num);
//                puzzle[i][j].setBackgroundResource(getImage(num));
            }
        }

    }

    public void display(){
        for(int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                puzzle[i][j].setBackgroundResource(imges[i][j]);
            }
        }
    }
    public int getImage(int num){
        switch (num){
            case 1:
                return R.drawable.p1;
            case 2:
                return  R.drawable.p2;
            case 3:
                return R.drawable.p3;
            case 4:
                return R.drawable.p4;
            case 5:
                return R.drawable.p5;
            case 6:
                return R.drawable.p6;
            case 7:
                return R.drawable.p7;
            case 8:
                return R.drawable.p8;
            case 9:
                return R.drawable.p9;
        }
        return 2;
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        gestureDetector.onTouchEvent(motionEvent);
        return true;
    }
}
